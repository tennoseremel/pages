# pages

Various static pages.

The code itself is licensed under CC0 1.0.

## fediverse-welcome, fediverse-welcome-skb

Text source: <https://fediland.github.io/index.html> (Alexey Skobkin), MIT license.

Pleroma screenshot source: <https://pleroma.social/>.
