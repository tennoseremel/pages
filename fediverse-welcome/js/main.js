'use strict';

function on_dom_loaded()
{
	let elements = document.querySelectorAll('.data-random-source');
	if (elements.length) {
		let source = elements[Math.floor(Math.random() * elements.length)];
		let c_result = document.getElementById('random-server-result');
		let cloned_anchor = source.cloneNode(true);
		cloned_anchor.setAttribute('class', 'random-server-button');
		cloned_anchor.textContent = "Зарегистрироваться на сервере " + cloned_anchor.textContent;
		c_result.appendChild(cloned_anchor);
		document.getElementById('random-server-premod').textContent = source.getAttribute('data-premod');
		document.getElementById('random-server-platform').textContent = source.parentNode.parentNode.previousElementSibling.textContent;
	}
}

document.addEventListener('DOMContentLoaded', on_dom_loaded, false);
